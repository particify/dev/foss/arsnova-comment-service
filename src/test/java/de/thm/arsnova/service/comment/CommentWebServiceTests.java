package de.thm.arsnova.service.comment;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled("This test does not run without external services.")
public class CommentWebServiceTests {

    @Test
    public void contextLoads() {
    }

}
